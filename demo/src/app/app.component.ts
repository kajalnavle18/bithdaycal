import { Component } from '@angular/core';
import {BirthDayservice} from '../app/services/birthday.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'demos';
  dateValue:any;
  birthdayValue: any;
  filterbirthDayValue: any=[];
  sundayArr:any=[];
  satArr:any=[];
  monArr:any=[];
  tueArr:any[];
  wedArr:any=[];
  ThurArr:any=[];
  FriArr:any=[];
  datefilterarr:any=[];
  errorMessage: boolean;
  

  constructor(private birthdayservice:BirthDayservice){
    //herer we can subscribe our observable methods which returns us json of birthday
   this.birthdayservice.get().subscribe(res=>{
     this.birthdayValue = res;
   });
  }
 
 arr:any=[];
  //it's use for display from youngest to oldest person date wise
   sortData(data) {
 
   this.datefilterarr.push(data.sort((a, b) => {
      return <any>new Date(a.birthday) - <any>new Date(b.birthday);
    }));
    this.arr =this.datefilterarr;
    
    console.log("datefilterarr",this.datefilterarr,this.arr)
  }
  jsonData:any
  jsonDataArr:any=[];
  //it's called when update button click and all the ui part will render
  onbtnClick(){
    this.jsonDataArr = JSON.parse(this.jsonData);
    this.datefilterarr=[];
    this.filterbirthDayValue=[];
    this.sundayArr=[{"name":"x Birthday","birthday":""}];
    this.monArr=[{"name":"x Birthday","birthday":""}];
    this.satArr=[{"name":"x Birthday","birthday":""}]
    this.tueArr=[{"name":"x Birthday","birthday":""}];
    this.wedArr=[{"name":"x Birthday","birthday":""}];
    this.ThurArr=[{"name":"x Birthday","birthday":""}];
    this.FriArr=[{"name":"x Birthday","birthday":""}];
    if(this.dateValue != "" ||this.dateValue != undefined)
   this.jsonDataArr.filter(response=>{
  
    let res = response.birthday.split("/");
    
    if(res[0] <=12){                                       // here the condition used because of date format was different so for that if month is less
                                                             //than equal to 12 then its enter into condtion.
     if(res[2] == this.dateValue){
       let newdate = response.birthday;
      let newdate1 = new Date(newdate);
    
      let getday = newdate1.getDay();
       var json={
         "birthday":response.birthday,
         "name":response.name,
         "day":getday
       }
       this.filterbirthDayValue.push(json);
      }
    }
 
     
    });

    for(let i =0;i<this.filterbirthDayValue.length;i++){
      if(this.filterbirthDayValue[i].day == 0){ 
        this.sundayArr.forEach(element => {
          if(element.name == "x Birthday"){
            this.sundayArr=[];
          }
          
        });
        this.sundayArr.push(this.filterbirthDayValue[i]);
        this.sortData(this.sundayArr);

      }else if(this.filterbirthDayValue[i].day == 6){
        this.satArr.forEach(element => {
          if(element.name == "x Birthday"){
            this.satArr=[];
          }
          
        });
          this.satArr.push(this.filterbirthDayValue[i]);
          this.sortData(this.satArr);
      }else if(this.filterbirthDayValue[i].day == 1){
        this.monArr.forEach(element => {
          if(element.name == "x Birthday"){
            this.monArr=[];
          }
          
        });
        this.monArr.push(this.filterbirthDayValue[i]);
        this.sortData(this.monArr);
      }
      else if(this.filterbirthDayValue[i].day == 2){
        this.tueArr.forEach(element => {
          if(element.name == "x Birthday"){
            this.tueArr=[];
          }
          
        });
        this.tueArr.push(this.filterbirthDayValue[i]);
        this.sortData(this.tueArr)
      }
      else if(this.filterbirthDayValue[i].day == 3){
        this.wedArr.forEach(element => {
          if(element.name == "x Birthday"){
            this.wedArr=[];
          }
          
        });
        this.wedArr.push(this.filterbirthDayValue[i]);
        this.sortData(this.wedArr);
      }
      else if(this.filterbirthDayValue[i].day == 4){
      this.ThurArr.forEach(element => {
        if(element.name == "x Birthday"){
          this.ThurArr=[];
        }
        
      });
        this.ThurArr.push(this.filterbirthDayValue[i]);
        this.sortData(this.ThurArr);
      }
      else if(this.filterbirthDayValue[i].day == 5){
        this.FriArr.forEach(element => {
          if(element.name == "x Birthday"){
            this.FriArr=[];
          }
          
        });
        this.FriArr.push(this.filterbirthDayValue[i]);
        this.sortData(this.FriArr);
      }
    }
    // console.log("sun",this.sundayArr,"sat",this.satArr,"mon",this.monArr,"tue",this.tueArr,"wed",this.wedArr,"thur",this.ThurArr,"fri",this.FriArr);
   
   };
 

   // it's use for display username with 1st letter
      titleCaseWord(word: string) {
      let newword= word.split(' ');
        return newword[0][0].toUpperCase() + newword[1][0].toUpperCase();
      }
  
  
}
