
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { map, retry, catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';

@Injectable({
    providedIn:'root'
})


export class BirthDayservice{
    constructor(private http:HttpClient){

    }

    //we read our birthday.json file locally using httpclient
    get() :Observable<any>{
    return this.http.get("./assets/birthday.json").pipe(retry(1),
    catchError(this.handleError),map(data => {
        return data;
    }));
}
handleError(error) {
    return throwError(error);
}

}